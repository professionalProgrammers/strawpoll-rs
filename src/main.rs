use std::fmt::Write;
use std::time::Duration;
use strawpoll::PollInfo;

fn format_poll_info(poll: &PollInfo) -> Result<String, std::fmt::Error> {
    let mut ret = String::new();
    for (i, answer) in poll.answers().iter().enumerate() {
        writeln!(
            &mut ret,
            "{}) {} | Id: {} | Votes: {}",
            i + 1,
            answer.text(),
            answer.id(),
            answer.votes()
        )?;
    }

    Ok(ret)
}

fn main() {
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let proxy_client = free_proxy_list::Client::new();

    loop {
        let proxy_list = rt.block_on(proxy_client.get_list()).unwrap();
        let good = rt.block_on(free_proxy_list::probe(
            proxy_list.iter().filter_map(|el| el.as_ref().ok()),
            Duration::from_secs(5),
        ));

        let valid_proxy_list = proxy_list
            .iter()
            .filter_map(|el| el.as_ref().ok())
            .zip(good.iter())
            .filter(|(_, good)| **good)
            .map(|(i, _)| i)
            .collect::<Vec<_>>();

        for info in valid_proxy_list.iter() {
            let client = {
                let proxy = reqwest::Proxy::all(&info.get_url()).unwrap();
                let proxy_client = reqwest::Client::builder()
                    .proxy(proxy)
                    .timeout(Duration::from_secs(10))
                    .build()
                    .unwrap();
                strawpoll::Client::from_reqwest(proxy_client)
            };

            let info = match rt.block_on(client.get_poll_info("s7zraas6")) {
                Ok(i) => i,
                Err(_) => {
                    continue;
                }
            };
            let r = format_poll_info(&info).unwrap();
            println!("{}", r);

            let vote = rt.block_on(client.vote("s7zraas6", 11933896)).unwrap();
            dbg!(vote);

            let info = rt.block_on(client.get_poll_info("s7zraas6")).unwrap();
            let r = format_poll_info(&info).unwrap();
            println!("{}", r);
        }
    }
}
