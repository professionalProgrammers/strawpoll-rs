use serde::{Deserialize, Serialize};
use std::collections::HashMap;

const USER_AGENT_STR: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36";

pub type StrawResult<T> = Result<T, StrawError>;

#[derive(Debug)]
pub enum StrawError {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),
}

impl From<reqwest::Error> for StrawError {
    fn from(e: reqwest::Error) -> Self {
        StrawError::Reqwest(e)
    }
}

#[derive(Default)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn from_reqwest(client: reqwest::Client) -> Self {
        Client { client }
    }

    pub async fn get_poll_info(&self, code: &str) -> StrawResult<PollInfo> {
        let url = format!("https://api2.strawpoll.com/poll/{}", code);
        let res = self.client.get(&url).send().await?;

        let status = res.status();
        if !status.is_success() {
            return Err(StrawError::InvalidStatus(status));
        }
        let info: PollInfoResponse = res.json().await?;
        Ok(info.poll)
    }

    pub async fn vote(&self, code: &str, answer: u64) -> StrawResult<serde_json::Value> {
        let res = self
            .client
            .post("https://api2.strawpoll.com/pollvote")
            .header(reqwest::header::USER_AGENT, USER_AGENT_STR)
            .json(&VoteRequest {
                poll_hash: String::from(code),
                checked_answers: answer,
                name: serde_json::Value::Null,
            })
            .send()
            .await?;
        let status = res.status();
        if !status.is_success() {
            return Err(StrawError::InvalidStatus(status));
        }
        Ok(res.json().await?)
    }
}

#[derive(Debug, Deserialize)]
pub struct PollInfoResponse {
    poll: PollInfo,
    message: String,
    success: u32,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize)]
pub struct PollInfo {
    is_valid: u32,
    meta: Option<serde_json::Value>,
    is_admin: u32,
    settings: Option<serde_json::Value>,
    answers: Vec<PollInfoAnswer>,
    hash: String,
    del: u32,
    question: String,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl PollInfo {
    pub fn answers(&self) -> &[PollInfoAnswer] {
        &self.answers
    }
}

#[derive(Debug, Deserialize)]
pub struct PollInfoAnswer {
    votes: u64,
    id: u64,
    text: String,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl PollInfoAnswer {
    pub fn votes(&self) -> u64 {
        self.votes
    }

    pub fn id(&self) -> u64 {
        self.id
    }

    pub fn text(&self) -> &str {
        &self.text
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct VoteRequest {
    poll_hash: String,
    checked_answers: u64,
    name: serde_json::Value,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct VoteResponse {
    success: u32,
    message: String,
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
